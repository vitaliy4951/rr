yc iam access-key create --service-account-name tfstate > ~/key.txt  # для Сервис Аккаунта tfstate получение ключа

export YC_TOKEN=`yc config list | grep token | awk '{print $2}'`
export YC_CLOUD_ID=`yc config list | grep cloud-id | awk '{print $2}'`
export YC_FOLDER_ID=`yc config list | grep folder-id | awk '{print $2}'`
export YC_ZONE=`yc config list | grep zone | awk '{print $2}'`
export TF_VAR_kid=`cat ~/key.txt | grep key_id | awk '{print $2}'`
export TF_VAR_sec=`cat ~/key.txt | grep secret | awk '{print $2}'`

sleep 2
terraform init -upgrade -reconfigure -backend-config="access_key=$TF_VAR_kid" -backend-config="secret_key=$TF_VAR_sec"
