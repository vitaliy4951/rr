#!/bin/bash
# Установка Helm repo gitlab-agent k8s
helm repo add gitlab https://charts.gitlab.io && helm repo update
# Установка Агента в кластер
export GL_AGENT=`cat ~/key.txt | grep ATOKEN | awk '{print $2}'`
helm upgrade --install kube-connect gitlab/gitlab-agent \
  --namespace gitlab-agent-kube-connect \
  --create-namespace \
  --set image.tag=v16.10.0-rc1 \
  --set config.token=$GL_AGENT \
  --set config.kasAddress=wss://kas.gitlab.com \
  --set replicas=2